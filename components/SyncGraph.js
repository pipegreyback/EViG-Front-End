import Rect, { Component } from "react"
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Brush, Legend, ResponsiveContainer } from 'recharts';

class SyncGraph extends Component {
  render() {
    const { datos } = this.props
    return (<div>
      <LineChart
        width={500}
        height={200}
        data={datos}
        syncId={datos[0].appliance}
        margin={{
          top: 10,
          right: 30,
          left: 0,
          bottom: 0
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="Time" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="ENERGY.Power" stroke="#82ca9d" fill="#82ca9d" />
        <Brush />
      </LineChart>
      <LineChart
        width={500}
        height={200}
        data={datos}
        syncId={datos[0].appliance}
        margin={{
          top: 10,
          right: 30,
          left: 0,
          bottom: 0
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="Time" />
        <YAxis />
        <Tooltip />
        <Line type="monotone" dataKey="ENERGY.Voltage" stroke="#82ca9d" fill="#82ca9d" />
      </LineChart>
    </div>
    )
  }
}
export default SyncGraph
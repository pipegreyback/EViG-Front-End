import React, { PureComponent } from 'react';
import {
    ResponsiveContainer,
    ComposedChart,
    Line,
    Area,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    LineChart,
} from 'recharts';

export default class SingleChart extends PureComponent {
    render() {
        const { datos } = this.props
        return (
            <div style={{ width: '100%', height: 300 }}>
                <ResponsiveContainer>
                    <LineChart
                        width={500}
                        height={400}
                        data={datos}
                        margin={{
                            top: 20,
                            right: 20,
                            bottom: 20,
                            left: 20,
                        }}
                    >
                        <CartesianGrid stroke="#f5f5f5" />
                        <XAxis dataKey="Time" scale="band" tick={<CustomizedAxisTick/>} />
                        <YAxis />
                        <Tooltip cursor={false} />
                        <Legend />
                        <Line type="monotone" dataKey="ENERGY.Power" stroke="#ff7300" dot={false} strokeWidth="3" />
                    </LineChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

class CustomizedAxisTick extends PureComponent {
    render() {
      const { x, y, stroke, payload } = this.props;
  
      return (
        <g transform={`translate(${x},${y})`}>
          <text x={0} y={0} dy={16} textAnchor="end" fill="#666" transform="rotate(-90)">
            {payload.value}
          </text>
        </g>
      );
    }
  }
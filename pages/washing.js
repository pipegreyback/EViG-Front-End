import SingleChart from "../components/SingleChart"

function Devices(props) {
    return (<div>
    <h2>Washing Machine</h2>
    <SingleChart datos={props.washingMachineArray}/>
    </div>
        )
}
export async function getStaticProps() {
    const washingMachine = await fetch("http://104.131.0.238:4200/appliance/washing machine")
    const washingMachineArray = await washingMachine.json()
    if (!washingMachineArray) {
        return {
            notFound: true
        }
    }
    return {
        props: { washingMachineArray }
    }
}

export default Devices;
import SyncGraph from "../components/SyncGraph"

function Devices(props) {
    return (<div>
    <h2>Kettle</h2>
    <SyncGraph id={props.kettleArray[0].appliance} datos={props.kettleArray}/>
    <h2>washing machine</h2>
    <SyncGraph id={props.microwaveArray[0].appliance} datos={props.microwaveArray}/>
    </div>
        )
}
export async function getStaticProps() {
    const kettle = await fetch("http://104.131.0.238:4200/appliance/kettle")
    const kettleArray = await kettle.json()
    const microwave = await fetch("http://104.131.0.238:4200/appliance/washing machine")
    const microwaveArray = await microwave.json()
    if (!kettleArray && !microwaveArray) {
        return {
            notFound: true
        }
    }
    return {
        props: { kettleArray, microwaveArray }
    }
}

export default Devices;